package android.example.com.cheruvuassignment;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.icu.lang.UScript;

//  By Default Room will use Class Name as Table Name, you can change it
//  by (tablename = TABLE_NAME) with annotation

//  Same goes for Colomn Names...

@Entity
public class User {

    @PrimaryKey(autoGenerate = true)
    private int UserID;

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getUserID() {
        return UserID;
    }

    private int SRNumber;
    private String FarmerName;
    private int FarmerAge;
    private String MandalName;
    private String VillageName;

    public int getSRNumber() {
        return SRNumber;
    }

    public void setSRNumber(int SRNumber) {
        this.SRNumber = SRNumber;
    }

    public String getFarmerName() {
        return FarmerName;
    }

    public void setFarmerName(String farmerName) {
        FarmerName = farmerName;
    }

    public int getFarmerAge() {
        return FarmerAge;
    }

    public void setFarmerAge(int farmerAge) {
        FarmerAge = farmerAge;
    }

    public String getMandalName() {
        return MandalName;
    }

    public void setMandalName(String mandalName) {
        MandalName = mandalName;
    }

    public String getVillageName() {
        return VillageName;
    }

    public void setVillageName(String villageName) {
        VillageName = villageName;
    }
}