package android.example.com.cheruvuassignment;

//  Data Access Objects uses for the Database Operations...

import android.arch.persistence.room.Insert;

@android.arch.persistence.room.Dao
public interface Dao {
    @Insert
    public void Insert(User user);
}
