package android.example.com.cheruvuassignment;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.lang.reflect.Method;
import java.util.HashMap;

public class FormActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    //  TextInput EditText
    TextInputEditText SerialNumber,FarmerName,FarmerAge;

    // Spinner For Selection of Mandal
    Spinner Mandal_Selection,Village_Selection;

    // Adapters for Data...
    ArrayAdapter<CharSequence> Mandaladapter;
    ArrayAdapter<CharSequence> Villageadapter;

    //  Submit Button
    AppCompatButton Submit;

    // Creating Database Object to store data into
    DataBase db;

    //  Creating User Data to set Data via setter
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        SerialNumber = findViewById(R.id.SRNo);
        FarmerName = findViewById(R.id.FName);
        FarmerAge = findViewById(R.id.FAge);

        Mandal_Selection = findViewById(R.id.Select_Mandal);
        Village_Selection = findViewById(R.id.Select_Village);
        Submit = findViewById(R.id.submitbtn);

        user = new User();

        // Setting Data from the list
        // Create an ArrayAdapter using the string array and a default spinner layout
        Mandaladapter = ArrayAdapter.createFromResource(this, R.array.Mandal_Selection_List, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        Mandaladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        Mandal_Selection.setAdapter(Mandaladapter);

        //  For Setting up Items...
        Mandal_Selection.setOnItemSelectedListener(this);

        FarmerAge.addTextChangedListener(new CheckAge());

        //  Initializing Database Object to use it to perform database operations...
        db = Room.databaseBuilder(getApplicationContext(), DataBase.class, "database-name").fallbackToDestructiveMigration().allowMainThreadQueries().build();

        // Visualizing Room Database in Browser...
        Stetho.initializeWithDefaults(this);

        //  On Submit Button Click
        Submit.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        user.setMandalName(Mandaladapter.getItem(position).toString());

        if(position==0){
            Villageadapter = ArrayAdapter.createFromResource(this, R.array.Mandal_1_Villages, android.R.layout.simple_spinner_item);
        }
        else if(position==1){
            Villageadapter = ArrayAdapter.createFromResource(this, R.array.Mandal_2_Villages, android.R.layout.simple_spinner_item);
        }
        else {
            Villageadapter = ArrayAdapter.createFromResource(this, R.array.Mandal_3_Villages, android.R.layout.simple_spinner_item);
        }

        // Apply the adapter to the spinner
        Village_Selection.setAdapter(Villageadapter);

        // Specify the layout to use when the list of choices appears
        Villageadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Villageadapter.notifyDataSetChanged();

        Village_Selection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user.setVillageName(Villageadapter.getItem(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(FormActivity.this, "No Villages Selected...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this, "No Item Selected...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        user.setFarmerName(FarmerName.getText().toString());
        user.setSRNumber(Integer.parseInt(SerialNumber.getText().toString()));
        user.setFarmerAge(Integer.parseInt(FarmerAge.getText().toString()));

        db.userDao().Insert(user);
    }

    // This Class is used to ensure that age is betweeen 1 to 100 only...
    class CheckAge implements TextWatcher {
        public void afterTextChanged(Editable s) {
            try {
                Log.d("Age", "input: " + s);
                if(Integer.parseInt(s.toString()) > 100)
                    s.replace(0, s.length(), "100");
            }
            catch(NumberFormatException nfe){}
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // Not used, details on text just before it changed
            // used to track in detail changes made to text, e.g. implement an undo
        }
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // Not used, details on text at the point change made
        }
    }
}
