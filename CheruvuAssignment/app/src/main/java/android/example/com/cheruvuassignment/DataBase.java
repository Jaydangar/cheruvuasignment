package android.example.com.cheruvuassignment;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {User.class}, version = 15,exportSchema = false)
public abstract class DataBase extends RoomDatabase {
    public abstract Dao userDao();
}