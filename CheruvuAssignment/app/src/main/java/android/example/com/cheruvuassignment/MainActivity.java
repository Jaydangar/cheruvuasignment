package android.example.com.cheruvuassignment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ImageView;

import jp.wasabeef.blurry.Blurry;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //  TextInput EditText
    TextInputEditText UserName;
    TextInputEditText Password;

    //  Signin Button
    AppCompatButton buttonSignIn;

    // Background ImageView
    ImageView BackGround;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UserName = findViewById(R.id.userName);
        Password = findViewById(R.id.password);

        BackGround = findViewById(R.id.BGImg);

        //  Getting Bitmap from Mipmap to use inside Blurry Background...
        Bitmap BackGroundBitmap = BitmapFactory.decodeResource(getResources(),R.mipmap.bgimg);

        // from Bitmap
        Blurry.with(this).radius(15).sampling(10).async().from(BackGroundBitmap).into(BackGround);

        buttonSignIn = findViewById(R.id.Signin);
        buttonSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Signin:
                if (UserName.getText().toString().equals("")) {
                    UserName.setError("UserName can't be null...");
                } else if (Password.getText().toString().equals("")) {
                    Password.setError("Password can't be null...");
                } else {
                    Intent FormIntent = new Intent(MainActivity.this,FormActivity.class);
                    startActivity(FormIntent);
                }
        }
    }
}
