# CheruvuAsignment

In This Assignment, I have used Material Design Principles as well as some of the third party libraries as well.

Third Party Library names and the reasons why i have used are below:

1. Room -> Alternative Library for SQLIte Database's Boiler Plate Implementation via SQLIte OpenHelper and it also Allows Robust Database Access. 

This is what google have to say about Room :-
The library helps you create a cache of your app's data on a device that's running your app. This cache, which serves as your app's single source of truth, allows users to view a consistent copy of key information within your app, regardless of whether users have an internet connection.

2. Stetho -> I have to visualize the Database contents of the Room, So i have used Stetho Library by Facebook.
More about Stetho -> https://github.com/facebook/stetho

3. Blurry -> For Background Blur
More About Blurry -> https://github.com/wasabeef/Blurry

4. Android Design Library for Material Components